var colors = [{
	color: "#feb409"
}, {
	color: "#9cd700"
}, {
	color: "#22a7ff"
}, {
	color: "#145897"
}]
var data;
var allNodes = [];
var container = d3.select("#container");

var padding = 50;
var containerWidth = (container.style("width").replace("px", ""));
var width = containerWidth;
var height = 400;
var currentCircle;
var currentLineGraph;
var currentScale = 1;
var isLikescale = true;


/* localization */
moment.locale('it');


/* graph svg */
var svg = container
	.append("svg")
	.attr("id", "viz")
	.attr("width", width)
	.attr("height", height);


var radio = container
	.append('div')
	.attr("class", "radio")
	.html('<form><label><input type="radio" name="mode" value="likes" checked> Like</label><label><input type="radio" name="mode" value="shares"> Condivisioni</label></form>')

radio.selectAll("input").on("change", function () {
	isLikescale = (this.value === "likes");
	drawGraph(true);
});


/* tooltip */
var tooltip = container.append("div")
	.attr("class", "tooltip")
	.style("opacity", 0)
	// .style("height", height + "px")

var myTimeFormatter = function (date) {
	return moment(date).format("MMM DD");
}
var parseDate = d3.time.format.utc("%Y-%m-%dT%H:%M:%S%Z").parse;


var currentDate = new Date();
/* Define Scales */
var xScale = d3.time.scale()
	.range([padding + 10, width - 25]);


var commentScale = d3.scale.linear().range([3, 20])
var sharesScale = d3.scale.linear().range([height - 25, 50])
var likeScale = d3.scale.linear().range([height - 25, 50]);


var zoom = d3.behavior.zoom()

var xAxis = d3.svg.axis()
	.ticks(d3.time.weeks, 2)
	.scale(xScale)
	.tickFormat(myTimeFormatter)
	.orient("bottom");

var xTicks = svg.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + (height - 25) + ")");

xTicks.call(xAxis);


var yAxis = d3.svg.axis()
	.scale(likeScale)
	.tickSize(width)
	.orient("right");

var yTicks = svg.append("g")
	.attr("class", "y axis");


var textYAxis = svg.append("g")
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("class", "label")
	.attr("x", -350)
	.attr("y", 20)
	.text("Likes");

svg.append("svg:rect")
	.attr("class", "pane")
	.attr("width", width)
	.attr("height", height)
	.call(zoom);


/* Line function used to connect nodes */
var lineFunction = d3.svg.line()
	.x(function (d) {
		return xScale(d.time);
	})
	.y(function (d) {
		return isLikescale ? likeScale(d.totallikes) : sharesScale(d.totalshares);
	})
	.interpolate("linear");


loadJSON();


function loadJSON() {
	d3.json("php/data.json", function (json) {
		data = json;
		createNodes();
	});

}

function createNodes() {


	data.forEach(function (feed, i) {

		feed.name = feed.name.substr(0, 1).toUpperCase() + feed.name.substr(1);
		feed.color = colors[i].color;
		colors[i].name = feed.name;
		if (feed.feed.length) {

			feed.totalcomments = 0;
			feed.totalshares = 0;
			feed.totallikes = 0;
			feed.nodes = [];
			feed.active = true;

			feed.feed.forEach(function (n) {
				n.name = feed.name;
				n.time = parseDate(n.created_time);
				n.color = feed.color;
				n.totalshares = (!n.shares) ? 0 : n.shares.count;
				n.totalcomments = (!n.comments) ? 0 : n.comments.summary.total_count;
				n.totallikes = (!n.likes) ? 0 : n.likes.summary.total_count;
				n.link = getLinkFromID(n);
				feed.totalshares += n.totalshares;
				feed.totalcomments += n.totalcomments;
				feed.totallikes += n.totallikes;
				//nodes that are comment to own nodes have no message AND the same id of the original post wtf... 
				if (n.message !== undefined) feed.nodes.push(n);
				/* allnodes is only used to calculate the axes range, we could have done it better */
				allNodes.push(n);

			});
			console.log(feed.nodes);

			xScale.domain(d3.extent(allNodes, function (d) {
				return d.time;
			}));
			zoom.x(xScale)
				.scaleExtent([1, 20])
				.on("zoom", function () {
					drawGraph(false);
					currentScale = d3.event.scale;
				});


			// /* set neighbors for navigation */
			feed.nodes.forEach(function (d, i) {
				if (i > 0) d.next = feed.nodes[i - 1];
				if (i < feed.nodes.length - 1) d.previous = feed.nodes[i + 1];
			})


			feed.group = svg.append("g")
				.attr("class", "feed");

			feed.lineGraph = feed.group.append("path")
				.attr("stroke-opacity", .2)
				.attr("stroke", feed.nodes[0].color)
				.attr("stroke-width", 2)
				.attr("fill", "none")
				.style("opacity", 0);

			feed.circles = feed.group.selectAll("circle")
				.data(feed.nodes)
				.enter()
				.append("circle")
				.attr("fill-opacity", .5)
				.style("fill", function (d) {
					return d.color;
				})
				.on("contextmenu", function () {
					return;
				})
				.on("mousedown", function (d) {
					resetCurrentPost();
					showPost(d, d3.select(this), feed.lineGraph);
				})
		} else {
			console.log("no data for " + feed.name);
		}


	});

	//hide loading
	document.getElementById("loading").className = "hiding";
	setDomains();
	drawLegend();
	drawGraph(true);
}

function drawGraph(updateScale) {

	// /* redraw axes */
	xAxis.ticks(currentScale < 2 ? d3.time.weeks : d3.time.days, 1)
	xTicks.call(xAxis);

	data.forEach(function (feed, i) {

		feed.circles
			.attr("cx", function (d) {
				return xScale(parseDate(d.created_time));
			})


		if (updateScale) {
			feed.lineGraph
				.transition()
				.duration(200)
				.attr("d", lineFunction(feed.nodes));
			yTicks.transition().duration(500).call(yAxis);
			textYAxis.text(isLikescale ? "Like" : "Condivisioni");


			feed.circles
				.transition()
				.duration(500)
				.attr("cy", function (d) {
					return isLikescale ? likeScale(d.totallikes) : sharesScale(d.totalshares);
				})
				.attr("r", function (d) {
					return commentScale(d.totalcomments);
				});
		} else {
			feed.lineGraph
				.attr("d", lineFunction(feed.nodes));

		}

	});


}

function resetCurrentPost() {
	if (currentLineGraph) {
		resetLineGraph(currentLineGraph);
	}

	if (currentCircle) {
		resetCircle(currentCircle);
	}
}


function resetLineGraph(lineGraph) {
	lineGraph.style("opacity", 0)
}

function resetCircle(circle) {
	circle.transition().duration(500).attr("fill-opacity", .5);
}

function drawLegend() {

	numeral.language('it', {
		delimiters: {
			thousands: '.',
			decimal: ','
		}
	});

	numeral.language('it');


	/* Legend */
	var legend = container
		.append('div')
		.attr('id', 'legend')


	var list = legend.selectAll('div')
		.data(data)
		.enter()
		.append('div');


	list.data(data)
		.append('input')
		.attr('checked', 'true')
		.style('background', function (d) {
			return d.color;
		})
		.attr('type', 'checkbox')
		.on('mouseup', checkboxClicked);

	list
		.append('div')
		.attr('class', 'riepilogo')
		.html(function (d) {
			return "<p>" + d.name + "</p>" + "<p>" + nf(d.feed.length) + " post</p>" + "<p>" + nf(d.totalcomments) + " commenti</p>" + "<p>" + nf(d.totalshares) + " condivisioni</p>" + "<p>" + nf(d.totallikes) + " likes</p>";
		})
		.style('color', function (d) {
			return d.color;
		})
}

// utility for formatting numbers
function nf(n) {
	return numeral(n).format();
}

function checkboxClicked(d) {

	/* modify circles that have been affected */
	if (currentLineGraph) {
		resetLineGraph(currentLineGraph);
	}

	var isChecked = !(d3.select(this).node().checked);

	data.forEach(function (k) {
		if (k.name === d.name) {
			k.active = isChecked;
		}
	})


	setDomains();
	drawGraph(true);
}

function setDomains() {
	var likes = [];
	var comments = [];
	var shares = [];

	data.forEach(function (feed) {
		if (feed.active) {
			feed.nodes.forEach(function (node) {
				likes.push(node.totallikes);
				comments.push(node.totalcomments);
				shares.push(node.totalshares);
			});
		}

		feed.group
			.style("pointer-events", function () {
				return feed.active ? "auto" : "none";
			})
			.transition()
			.duration(200)
			.style("opacity", function () {
				return feed.active ? 1 : 0;
			});

	});


	commentScale.domain([d3.min(comments), d3.max(comments)])
	sharesScale.domain([d3.min(shares), d3.max(shares)])
	likeScale.domain([d3.min(likes), d3.max(likes)])

	if (isLikescale) {
		yAxis.scale(likeScale)
	} else {
		yAxis.scale(sharesScale)
	}


}

function showPost(d, node, lineGraph) {

	resetCurrentPost();
	currentCircle = node;
	currentLineGraph = lineGraph;

	lineGraph.style("opacity", 1)
	node.transition().duration(200).attr("fill-opacity", 1);

	tooltip.style("opacity", 1)
		.transition()
		.duration(200)
	tooltip.html("<h1 style='color:" + d.color + "'>" + d.name + "</h1>" +
		"<p class='meta'><span>commenti:</span> " + nf(d.totalcomments) + "</p>" +
		"<p class='meta'><span>condivisioni:</span> " + nf(d.totalshares) + "</p>" +
		"<p class='meta'><span>likes:</span> " + nf(d.totallikes) + "</p>" +
		"<div id='navigation'></div>" +
		((d.picture) ? "<img src='" + d.picture + "' />" : "") +
		"<p>" + d.message + "</p>" +
		"<p><a class='overlay' href='" + d.link + "' target='_blank'>Vai al post originale</a></p>" +
		"<p class='date'>postato il " + moment(d.created_time).format('LLL') + "</p>"
	);

	var nav = tooltip.select("#navigation");

	if (d.previous.id) {
		nav.append("a").attr("href", "#").html("← precedente | ").on("mousedown", function (e) {
			showPost(d.previous, svg.select("#id" + d.previous.id), lineGraph);
		});
	}


	if (d.next.id) {
		nav.append("a").attr("href", "#").html("successivo →").on("mousedown", function (e) {
			showPost(d.next, svg.select("#id" + d.next.id), lineGraph);
		});
	}
}


/* helpers */
function getLinkFromID(n) {
	var a = n.id.split("_");
	var user = a[0];
	var post = a[1];
	return "http://www.facebook.com/" + user + "/posts/" + post;

}