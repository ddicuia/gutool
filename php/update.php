
<?php 
// error_reporting(E_ALL); 

date_default_timezone_set('Europe/Rome');



require 'vendor/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;



// when token expires go grab a new one from the debug access token in the facebook api
FacebookSession::setDefaultApplication('875661485795244','f160df14c72e26658b8d6e6ac8a139fb');
$session = new FacebookSession('CAAMcaNPN56wBAPYlHoN0mkYj3zUUwonA4tGndnU9ZBTtz3wWDUn0DhCBJJLHc7QQZBsZCVxd78l5ZBXanScSdFdRZB0vKYsoYMTJvQiDZA0d1ps1ZAnBBNNUnqpnCj6hxtlgCueSxZAS68ZA5uZCbCgtH21ZCm8ufPcubAHw1o0xZCF2z76u6k3e1vTCSndgK6PyolsZD');
$access_token = $session->getAccessToken();
try {
	$session->validate();
} catch (FacebookRequestException $ex) {
  // Session not valid, Graph API returned an exception with the reason.
	echo $ex->getMessage();
} catch (\Exception $ex) {
  // Graph API returned info, but it may mismatch the current app or have expired.
	echo $ex->getMessage();
}





$result = array();

// $result[]  = array('name' => 'minervini', 'feed' => getFacebookFeed(151567064757));
$result[]  = getFacebookFeed('renzi', 113335124914);
$result[]  = getFacebookFeed('salvini', 252306033154);
$result[]  = getFacebookFeed('grillo', 56369076544);

// $result[]  = array('name' => 'open', 'feed' => getFacebookFeed(526239727400034));
// $result[]  = array('name' => 'emiliano', 'feed' => getFacebookFeed(53505098083));
// $result[]  = array('name' => 'stefàno', 'feed' => getFacebookFeed(318853806269));
// $result[]  = array('name' => 'vendola', 'feed' => getFacebookFeed(38771508894));

$jsonEncoded = json_encode($result);
$fp = fopen('data.json', 'w');
fwrite($fp, $jsonEncoded);
fclose($fp);

// echo $jsonEncoded;



function getFacebookFeed($name, $id) {
	global $session;
	try {
		//http://stackoverflow.com/questions/17755753/how-to-get-likes-count-when-searching-facebook-graph-api-with-search-xxx
		$fields ='picture,message,shares,comments.limit(1).summary(true),likes.limit(1).summary(true)';
		

		$since = date('Y-m-d', strtotime('-1 month'));
		$until = date('Y-m-d', strtotime('now'));
		
		$result = array();
		$pages = 0;
		$limit = 250;

		$request = new FacebookRequest($session, 'GET', "/$id/posts?limit=$limit&since=$since&fields=$fields");
		// $request = new FacebookRequest($session, 'GET', "/$id/posts?limit=$limit&fields=$fields");

		$going = true;
		do {
			// echo "requesting ". $name. " page $pages";
			echo "requesting ". $name. " page $pages" ." since ". ($since)  ." until " .($until) . "<br>";
			
			$response = $request->execute();
			$graphObject = $response->getGraphObject();
			$data = $graphObject->getProperty('data');
			if(is_object($data) ){
				$new_result = $data->asArray();
				$result = array_merge($result, $new_result);
				$pages++;
				$request = $response->getRequestForNextPage();
			} else {
				$going = false;
			}
		} while ($going);

		echo "total: ".sizeof($result) . "<br>";


		return array('name' =>  $name, 'feed' => $result);

	} catch (FacebookRequestException $e) {
		// echo $e;
		return NULL;
	} catch (Exception $e) {
		// echo $e;
		return NULL;
	}
}

?>
