
<?php 

/*
	Find common commenters
*/

	$flusso = json_decode( file_get_contents("../commenters.json"),true);


	/* Create commenters arrays */

	foreach ($flusso as &$value) {
		
		$value["commentersNames"] = array();

		foreach ($value["commenters"] as $a) {
			if (count( $a["comments"]) >0) {
				foreach ($a["comments"] as $b) {
					foreach ($b as $c) {
						if(is_array($c) && array_key_exists("from", $c)) {
							$value["commentersNames"][]= $c["from"]["name"];
						} 
					}
				}
			}
		}




		$c = array_count_values($value["commentersNames"]); 
		arsort($c);

		$name = $value["name"];

		$i =0;
		echo "Commentatori più attivi per $name\n";
		foreach ($c as $k=>$d) {
			echo "$k ($d post) \n";
			if(++$i > 10) break;
		}

		$value["commentersNames"]= array_unique($value["commentersNames"]);

		echo "\n";

	}

	/* Common commenters */

	$keys = array_keys($flusso);

	$comuni = (array_intersect($flusso[$keys[0]]["commentersNames"], $flusso[$keys[1]]["commentersNames"], $flusso[$keys[2]]["commentersNames"]));

	echo "Commentatori in comune\n";
	foreach ($comuni as $value) {
		echo "$value \n";
	}

	


	?>
