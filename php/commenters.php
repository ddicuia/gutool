
<?php 

/*
	Create JSON with last 3 months' commenters
*/

date_default_timezone_set('Europe/Rome');


require 'vendor/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

FacebookSession::setDefaultApplication('875661485795244','f160df14c72e26658b8d6e6ac8a139fb');

$session = new FacebookSession('CAAMcaNPN56wBAC3UmrfzZBgh4sJudHalmGPEJ0VI2nuumrQAaNuTJZAxzjKtIG03l9kfhZCnMgevZCF0WxZCvYQyb8UndWX3YuCKmbc6gvldqqD9VWmEYZCCs1jZBdbkQ4CAgtuyX3ZBMKlnDQOMVTaWkq4R1RNPAnB6bbpzEaGZCjNECJrUQVng01tv4oK5FSdpOXdCaC4MB4xZBuJzYLsXvA');


$result = array();

$result[]  = array('name' => 'minervini', 'commenters' => getCommenters(151567064757));
$result[]  = array('name' => 'emiliano', 'commenters' => getCommenters(53505098083));
$result[]  = array('name' => 'stefàno', 'commenters' => getCommenters(318853806269));

$jsonEncoded = json_encode($result);
$fp = fopen('../commenters.json', 'w');
fwrite($fp, $jsonEncoded);
fclose($fp);

echo $jsonEncoded;



function getCommenters($id) {
	global $session;
	try {
		$fields ='comments.summary(true).filter(stream).limit(1000){from}';
		$since = date('Y-m-d', strtotime('-3 months'));

		$result = array();
		$offset = 0;
		$limit = 250;

		$request = new FacebookRequest($session, 'GET', "/$id/posts?since=$since&limit=$limit&offset=$offset&fields=$fields");
		$response = $request->execute()->getGraphObject()->asArray();
		$result = array_merge($result, $response);

		while(in_array("paging", $response) && array_key_exists("next", $response["paging"])) {
			$offset += $limit;
			$request = new FacebookRequest($session, 'GET', "/$id/posts?limit=$limit&offset=$offset&fields=$fields");
			$response = $request->execute()->getGraphObject()->asArray();
			$result = array_merge($result, $response);
		}

		return $result["data"];

	} catch (FacebookRequestException $e) {
		echo $e;
		return NULL;
	} catch (Exception $e) {
		echo $e;
		return NULL;
	}
}


?>
